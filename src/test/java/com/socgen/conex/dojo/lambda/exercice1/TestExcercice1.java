package com.socgen.conex.dojo.lambda.exercice1;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("test des streams et lambdas")
public class TestExcercice1 {
	IHumanDispatcher dispatcher;
	
	private static final List<Human> humains = Arrays.asList(
			Human.builder().nom("Johnson").prenom("Robert").age(10).achievements(Arrays.asList("naitre", "école")).build(),
			Human.builder().nom("Lipvig").prenom("Zelda").achievements(Arrays.asList("naitre", "école", "bac", "anglais")).age(20).build(),
			Human.builder().nom("Robson").prenom("John").achievements(Arrays.asList("naitre", "école", "bac", "job")).age(30).build(),
			Human.builder().nom("Lemoine").prenom("Jean").achievements(Arrays.asList("naitre", "école", "bac", "job", "enfants")).age(40).build(),
			Human.builder().nom("Lepretre").prenom("Richard").achievements(Arrays.asList("naitre", "école", "bac", "job", "voyage", "anglais")).age(50).build(),
			Human.builder().nom("Lionheart").prenom("Cunegonde").achievements(Arrays.asList("naitre", "école", "bac", "job", "enfants", "rolex")).age(60).build(),
			Human.builder().nom("Oldman").prenom("Garry").achievements(Arrays.asList("naitre", "école", "bac", "job", "enfants", "voyage", "petits enfants")).age(70).build()
		);
	
	@BeforeEach
	public void beforeEach() {
		dispatcher = Factory.getHumanDispatcher(humains);
	}
	
	@Test
	@DisplayName("doit contenir \'Lionheart\'")
	public void testContientNom() {
		assertTrue(dispatcher.contientNom("Lionheart"));
		assertFalse(dispatcher.contientNom("Ursula"));
		assertFalse(dispatcher.contientNom("Zelda"));
	}
	
	@Test
	@DisplayName("doit contenir les 7 prénoms")
	public void testListePrenoms() {
		assertEquals(
			Arrays.asList(
				"Robert", "Zelda", "John",
				"Jean", "Richard", "Cunegonde",
				"Garry"
			),	
			dispatcher.listePrenoms());
	}
	
	@Test
	@DisplayName("lister et trier les prénoms par ordre alphabétique")
	public void testListeEtTriePrenoms() {
		assertEquals(
			Arrays.asList(
				"Cunegonde", "Garry", "Jean",
				"John", "Richard", "Robert",
				"Zelda"
			),	
			dispatcher.listeEtTriePrenoms());
	}
	
	@Test
	@DisplayName("filtrer par âge minimum = 50")
	public void testFiltrerParAgeMinimum() {
		List<Human> filtered = dispatcher.filtrerParAgeMinimum(50);
		assertThat("Les humains sont bien filtrés",
				filtered,
			containsInAnyOrder(
				humains.get(4),
				humains.get(5),
				humains.get(6)
			)
		);
		assertThat("Les humains sont bien filtrés",
			filtered,
			not(containsInAnyOrder(
				humains.get(0),
				humains.get(1),
				humains.get(2),
				humains.get(3)
			)));
	}
	
	@Test
	@DisplayName("récupérer âge maximum")
	public void testGetAgeMaximum() {
		assertEquals(70, dispatcher.getAgeMaximum());
	}
	
	@Test
	public void testCompterSuccees() {
		List<Pair<String, Integer>> succes = dispatcher.compterSuccees();

		assertNotNull(succes);
		assertThat("naitre non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("naitre")), hasProperty("right", is(7)))));
		assertThat("école non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("école")), hasProperty("right", is(7)))));
		assertThat("bac non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("bac")), hasProperty("right", is(6)))));
		assertThat("anglais non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("anglais")), hasProperty("right", is(2)))));
		assertThat("job non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("job")), hasProperty("right", is(5)))));
		assertThat("enfants non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("enfants")), hasProperty("right", is(3)))));
		assertThat("voyage non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("voyage")), hasProperty("right", is(2)))));
		assertThat("rolex non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("rolex")), hasProperty("right", is(1)))));
		assertThat("petits enfants non présent dans les succes ou occurence fausse", succes, hasItem(allOf(hasProperty("left", is("petits enfants")), hasProperty("right", is(1)))));
	}

	@Test
	public void testCustom() {
		assertEquals(4, dispatcher.customSelect(human -> human.getAchievements().size() > 4).size());
	}
}
