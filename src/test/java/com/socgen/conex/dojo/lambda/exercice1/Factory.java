package com.socgen.conex.dojo.lambda.exercice1;

import java.util.List;

import com.socgen.conex.dojo.lambda.exercice1.solution.HumanDispatcherSolution;

public class Factory {

	private static boolean withSolution = true;
	
	public static IHumanDispatcher getHumanDispatcher(List<Human> humans) {
		if(withSolution) {
			return new HumanDispatcherSolution(humans);
		}
		else {
			return new HumanDispatcher(humans);
		}
	}
}
