package com.socgen.conex.dojo.lambda.exercice1;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public class HumanDispatcher implements IHumanDispatcher {

	private List<Human> humans;

	public HumanDispatcher(List<Human> humans) {
		this.humans = humans;
	}
	
	public boolean contientNom(String nom) {
		/**
		 * Votre solution ici
		 */
		return false;
	}
	
	public List<Human> filtrerParAgeMinimum(long minAge) {
		/**
		 * Votre solution ici
		 */
		return null;
	}


	public List<String> listePrenoms() {
		/**
		 * Votre solution ici
		 */
		return null;
	}

	public List<String> listeEtTriePrenoms() {
		/**
		 * Votre solution ici
		 */
		return null;
	}
	
	public long getAgeMaximum() {
		/**
		 * Votre solution ici
		 */
		return 0;
	}

	@Override
	public List<Pair<String, Integer>> compterSuccees() {
		/**
		 * Votre solution ici
		 */
		return null;
	}

	@Override
	public List<Human> customSelect(Selector selector) {
		/**
		 * Votre solution ici
		 */
		return null;
	}
	
}
