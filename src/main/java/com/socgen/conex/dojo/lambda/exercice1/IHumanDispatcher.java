package com.socgen.conex.dojo.lambda.exercice1;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

public interface IHumanDispatcher {
	
	/**
	 * @param le nom recherché
	 * @return présence ou non du nom
	 */
	boolean contientNom(String nom);
	
	
	/**
	 * @param humans une liste d'humains
	 * @param minAge la barrière d'age
	 * @return la liste des humains ayant plus de {age}
	 */
	List<Human> filtrerParAgeMinimum(long minAge);
	
	/**
	 * @return la liste des prénoms dans le même ordre
	 */
	List<String> listePrenoms();
	
	/**
	 * @return la liste des prénoms triée par ordre alphabétique
	 */
	List<String> listeEtTriePrenoms();
	
	/**
	 * Retourne l'age maximum parmis les humains dans {humans}
	 * @return
	 */
	long getAgeMaximum();
	
	/**
	 * Retourne la liste des succés et leurs nombre d'occurence
	 */
	List<Pair<String, Integer>> compterSuccees();
	
	/**
	 * Select avec des critères particuliers
	 * @param selector
	 * @return
	 */
	List<Human> customSelect(Selector selector);
}
