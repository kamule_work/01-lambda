package com.socgen.conex.dojo.lambda.exercice1.solution;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.counting;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.socgen.conex.dojo.lambda.exercice1.Human;
import com.socgen.conex.dojo.lambda.exercice1.IHumanDispatcher;
import com.socgen.conex.dojo.lambda.exercice1.Selector;

public class HumanDispatcherSolution implements IHumanDispatcher{

	private List<Human> humans;

	public HumanDispatcherSolution(List<Human> humans) {
		this.humans = humans;
	}
	
	public List<Human> filtrerParAgeMinimum(long minAge) {
		return humans.stream()
				.filter(human -> human.getAge() >= minAge)
				.collect(Collectors.toList());
	}

	public long getAgeMaximum() {
		return humans.stream()
				.max(
					Comparator.comparing(h -> h.getAge())
				)
				.get()
				.getAge();
	}

	public boolean contientNom(String nom) {
		return humans.stream()
				.anyMatch(human -> human.getNom().equals(nom));
	}

	public List<String> listePrenoms() {
		return humans.stream()
				.map(human -> human.getPrenom())
				.collect(Collectors.toList());
	}

	public List<String> listeEtTriePrenoms() {
		return humans.stream()
				.map(human -> human.getPrenom())
				.sorted()
				.collect(Collectors.toList());
	}
	
	@Override
	public List<Pair<String, Integer>> compterSuccees() {
		return humans.stream()
			.flatMap(human -> human.getAchievements().stream()) // passage � plat des succes
			.collect(Collectors.groupingBy(x -> x, counting())) // transformation en map contenant le nombre d'occurence
			.entrySet().stream() // stream de la map
			.map(entry -> new ImmutablePair<>(entry.getKey(), Integer.valueOf(entry.getValue().toString()))) // passage de l'entry � la pair
			.collect(Collectors.toList()); // passage en liste
	}
	
	@Override
	public List<Human> customSelect(Selector selector) {
		return humans.stream()
			.filter(human -> selector.execute(human))
			.collect(Collectors.toList());
	}

}
