package com.socgen.conex.dojo.lambda.exercice1;

import java.util.List;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @Builder @EqualsAndHashCode @ToString
public class Human {
	private String nom;
	private String prenom;
	private int age;
	private List<String> achievements;
}
