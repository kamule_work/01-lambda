package com.socgen.conex.dojo.lambda.exercice1;

public interface Selector {
	/**
	 * Selectionne un humain selon des critères spécialisés
	 * @param human
	 * @return
	 */
	public boolean execute(Human human);
}
