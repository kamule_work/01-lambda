**Session sur les lambda**

Nous allons voir ici comment utiliser les lambda, dans quel cas, comment les d�bugger et comment en cr�er.

---

## Installation

Ce projet se concentre sur les lambda en Java. Pour simplifier le code les exemples sont construits avec [lombook](https://projectlombok.org/).

Si vous ne l'avez pas fait merci de patcher votre eclipse [en suivant la proc�dure] suivante(https://projectlombok.org/setup/eclipse).

Pour le reste, maven et Java 8 (dispo sur AppsOnDemands) suffisent � lancer les tests.

---

## Exercice 1

Ici nous allons introduire les lambda et voir leurs cas d'utilisation classique des lambda en manipulant des collections.

-- 

Les solutions aux exercices sont disponibles dans le sous package `.solution` de chaque exercice.